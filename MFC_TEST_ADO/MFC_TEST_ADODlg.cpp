﻿
// MFC_TEST_ADODlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFC_TEST_ADO.h"
#include "MFC_TEST_ADODlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern CMFCTESTADOApp theApp;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCTESTADODlg 对话框




CMFCTESTADODlg::CMFCTESTADODlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_TEST_ADO_DIALOG, pParent)
	, m_nID(0)
	, m_nAge(0)
	, m_strUserName(_T(""))
	, m_tBirthday(COleDateTime::GetCurrentTime())
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	m_bSuccess = FALSE;

	m_nCurSelect = -1;

	m_bAutoSave = TRUE;
}

void CMFCTESTADODlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_ID, m_nID);
	DDX_Text(pDX, IDC_EDIT_AGE, m_nAge);
	DDX_Text(pDX, IDC_EDIT_USER, m_strUserName);
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER1, m_tBirthday);
	DDX_Control(pDX, IDC_USER_LIST, m_UserList);
}

BEGIN_MESSAGE_MAP(CMFCTESTADODlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTN_ADD, &CMFCTESTADODlg::OnBnClickedBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DELETE, &CMFCTESTADODlg::OnBnClickedBtnDelete)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_USER_LIST, &CMFCTESTADODlg::OnLvnItemchangedUserList)
	ON_BN_CLICKED(IDOK, &CMFCTESTADODlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CMFCTESTADODlg::OnBnClickedCancel)
END_MESSAGE_MAP()


// CMFCTESTADODlg 消息处理程序

BOOL CMFCTESTADODlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	//::SendMessage(m_UserList.m_hWnd, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);
	//////////为列表控件添加列//////////
	
	// 为列表控件添加列
	m_UserList.SetExtendedStyle(m_UserList.GetExtendedStyle()|LVS_EX_FULLROWSELECT| LVS_EX_GRIDLINES);
	m_UserList.InsertColumn(0,_T("ID"),LVCFMT_LEFT,60);
	m_UserList.InsertColumn(1, _T("用户名"), LVCFMT_LEFT,100);
	m_UserList.InsertColumn(2, _T("年龄"), LVCFMT_LEFT,60);
	m_UserList.InsertColumn(3, _T("生日"), LVCFMT_LEFT,120);

	// 读取数据库中的信息添加到列表控件
	int nItem;
	_variant_t vUserName, vID, vAge, vBirthdayr;

	try
	{
		// 创建一个新的连接数据库实例
		m_pRecordset.CreateInstance(_T("ADODB.Recordset"));
		// 使用SQL语句,用来查询users这个表的内容
		m_pRecordset->Open("SELECT * FROM users",_variant_t((IDispatch*)theApp.m_pConnnect,TRUE),adOpenStatic,adLockOptimistic,adCmdText);
		m_bSuccess = TRUE;
		// EOF(End of File:指示当前位置位于Recordset对象的最后一个记录之后)
		// 使用BOF(Before of File):bof表示rs当前的指针是指在了数据集的前面;使用BOF和EOF可用于判断Recordset对象是否包含记录
		// 或者从一个记录移到另一个记录时是否超出Recordset对象的限制。
		while(!m_pRecordset->adoEOF)  
		{
			// GetCollect()函数的作用:用来获取收集的字符串,例如就是用来获取_T("")字符串的
			vID = m_pRecordset->GetCollect(_T("id"));
			vUserName = m_pRecordset->GetCollect(_T("username"));
			vAge = m_pRecordset->GetCollect(_T("age"));
			vBirthdayr = m_pRecordset->GetCollect(_T("birthday"));
			// 在控件中插入新行
			nItem = m_UserList.InsertItem(0xffff,(_bstr_t)vID);
			// 在已经存在的行中设置信息
			m_UserList.SetItem(nItem, 1, 1, (_bstr_t)vUserName,NULL,0,0,0);
			m_UserList.SetItem(nItem, 2, 1, (_bstr_t)vAge, NULL, 0, 0, 0);
			m_UserList.SetItem(nItem, 3, 1, (_bstr_t)vBirthdayr, NULL, 0, 0, 0);
			m_pRecordset->MoveNext(); 
		}
	}
	catch (_com_error e)
	{
		AfxMessageBox(_T("读取数据库失败!"));
	}
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCTESTADODlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCTESTADODlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCTESTADODlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCTESTADODlg::OnBnClickedBtnAdd()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData();
	if (m_strUserName.GetLength() > 0)
	{
		// 创建可更新Recordset对象的新纪录
		m_pRecordset->AddNew();
		// 在65535行中插入_T("")
		m_nCurSelect = m_UserList.InsertItem(0xffff,_T(""));
		SaveData();
		m_UserList.SetItemState(m_nCurSelect,LVIS_SELECTED| LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_UserList.SetHotItem(m_nCurSelect);
		m_UserList.SetFocus();
	}
	else
	{
		AfxMessageBox(_T("请输入用户名"));
	}
}


void CMFCTESTADODlg::OnBnClickedBtnDelete()
{
	// TODO: 在此添加控件通知处理程序代码
	m_bAutoSave = FALSE;
	if (m_nCurSelect >= 0)
	{
		// 删除当前选中行
		m_UserList.DeleteItem(m_nCurSelect);
		int nCount = m_UserList.GetItemCount();
		if (nCount <= m_nCurSelect)
			m_nCurSelect = nCount - 1; // 删除之后重新设置选中行
		// 设置当前指针指向的记录会被删除
		m_pRecordset->Delete(adAffectCurrent);
		// 将当前记录集指针移到下一个记录
		m_pRecordset->MoveNext();
		if (nCount > 0)
			LoadData();
		// 设置当前选中行,为选中状态
		m_UserList.SetItemState(m_nCurSelect, LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
		m_UserList.SetFocus();
	}
	m_bAutoSave = TRUE;
}


void CMFCTESTADODlg::OnLvnItemchangedUserList(NMHDR* pNMHDR, LRESULT* pResult)
{
	// NMHDR:包含有关此通知消息的信息结构体
	// LPNMLISTVIEW:专属于ListView类型的通知消息结构体,包含了NMHDR,换一个控件会有不同的形式
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	// 判断是否选择当前控件的行
	if (pNMLV->uNewState & LVIS_SELECTED)
	{
		UpdateData();
		SaveData();
		// 设置当前行为选中行
		m_nCurSelect = pNMLV->iItem;
		LoadData();
	}
	*pResult = 0;
}


void CMFCTESTADODlg::OnBnClickedOk()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_bSuccess)
	{
		// Update:保存对于对于当前记录集对象,即m_pRecordset数据的修改
		m_pRecordset->Update();
		// 关闭一个对象
		m_pRecordset->Close();
	}
	CDialogEx::OnOK();

}


void CMFCTESTADODlg::OnBnClickedCancel()
{
	// TODO: 在此添加控件通知处理程序代码
	CDialogEx::OnCancel();
}

// 获取当前指定行中数据库中的记录显示在控件中
void CMFCTESTADODlg::LoadData()
{
	// 将当前记录集指针移动到第一个书签的位置
	m_pRecordset->Move(m_nCurSelect, _variant_t((long)adBookmarkFirst));

	m_vID = m_pRecordset->GetCollect(_T("id"));
	m_vUserName = m_pRecordset->GetCollect(_T("username"));
	m_vAge = m_pRecordset->GetCollect(_T("age"));
	m_vBirthday = m_pRecordset->GetCollect(_T("birthday"));

	m_nID = m_vID.lVal;
	m_strUserName = (LPCTSTR)(_bstr_t)m_vUserName;
	m_nAge = m_vAge.lVal;
	m_tBirthday = m_vBirthday;
	UpdateData(FALSE);
}


// 将当前控件中的值填加到数据库中
void CMFCTESTADODlg::SaveData()
{
	if (!m_pRecordset->adoEOF && m_nCurSelect >= 0 && m_bAutoSave)
	{
		m_vID = (long)m_nID;
		m_vUserName = m_strUserName;
		m_vAge = (long)m_nAge;
		m_vBirthday = m_tBirthday;

		// PutCollect(A,B)函数的作用是将B中的数据添加到数据库A中
		m_pRecordset->PutCollect(_T("id"),m_vID);
		m_pRecordset->PutCollect(_T("username"), m_vUserName);
		m_pRecordset->PutCollect(_T("age"), m_vAge);
		m_pRecordset->PutCollect(_T("birthday"), m_vBirthday);

		// 将当前修改的值插入到当前选择行中
		m_UserList.SetItem(m_nCurSelect,0,LVIF_TEXT,(_bstr_t)m_vID,NULL,0,0,0);
		m_UserList.SetItem(m_nCurSelect, 1, LVIF_TEXT, (_bstr_t)m_vUserName, NULL, 0, 0, 0);
		m_UserList.SetItem(m_nCurSelect, 2, LVIF_TEXT, (_bstr_t)m_vAge, NULL, 0, 0, 0);
		m_UserList.SetItem(m_nCurSelect, 3, LVIF_TEXT, (_bstr_t)m_vBirthday, NULL, 0, 0, 0);
	}
}