﻿
// MFC_TEST_ADODlg.h: 头文件
//

#pragma once
#include"framework.h"


// CMFCTESTADODlg 对话框
class CMFCTESTADODlg : public CDialogEx
{
// 构造
public:
	CMFCTESTADODlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_TEST_ADO_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

public:

	afx_msg void OnBnClickedBtnAdd();
	afx_msg void OnBnClickedBtnDelete();
	afx_msg void OnLvnItemchangedUserList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedCancel();

	// 加载数据
	void LoadData();
	// 保存修改
	void SaveData();


	// by 2022/11/6 m_pRecordset 指向记录集合的指针,从数据库查询得到的结果放在m_pRecordset中
	// Recordset 类重载了-> 所以允许它像指针一样去调用
	_RecordsetPtr m_pRecordset;

	UINT m_nID;
	UINT m_nAge;
	CString m_strUserName;
	// DataTime 控件成员变量
	COleDateTime m_tBirthday;
	// CListCtrl 控件
	CListCtrl m_UserList;
	// 用于判断是否成功读取数据库数据
	BOOL m_bSuccess;
	// 获取CListCtrl 控件中当前选择的行
	int m_nCurSelect;
	// 判断在添加数据或者选择数据后是否保存数据
	BOOL m_bAutoSave;

	// 定义四个 _variant_t 类对象
	_variant_t m_vID, m_vUserName, m_vAge, m_vBirthday;
};
